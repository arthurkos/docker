Créer un serveur web qui affiche le fichier index.html
Le fichier index.html est produit par un conteneur qui insère dans le fichier la date toutes les 60 m

Pour générer le fichier index.html toutes les X minutes nous allons créer un conteneur Centos7 avec un Dockerfile qui va nous permettre grâce a un script de générer ce fichier dans /data du conteneur. Et par la suite grâce à un docker-compose nous allons partager le index.html au server web.

Le docker-compose:

- Lancement de notre conteneur qui fait le script.
- Mappage du volume tp.
- Lancement d’un conteneur nginx.
- On map le volume tp à l’endroit de la conf nginx pour les fichiers html.
- Et on le rend visible sur le port 8080.
- Création d’un volume tp qui sera visible entre les conteneurs
